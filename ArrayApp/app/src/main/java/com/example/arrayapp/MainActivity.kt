package com.example.arrayapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Spinner

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val studentName = arrayOf("Shilpi", "Arman", "Ansar", "Akash")
        val studentNameArraySize = studentName.size

        Log.d("studentNameArraySize", studentNameArraySize.toString())
        val nameSpinner = findViewById<Spinner>(R.id.name_spinner)
        val nameList = findViewById<ListView>(R.id.name_list)


        val spinnerAdapter = ArrayAdapter(this, R.layout.spinner_item_layout,studentName)
        nameList.adapter = spinnerAdapter
        nameSpinner.adapter = spinnerAdapter
    }
}